package it.unibo.ds.grpc.greeting;

import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Client {

    private static final HelloRequest giovanni = HelloRequest.newBuilder().setName("Giovanni").build();
    private static final HelloRequest andrea = HelloRequest.newBuilder().setName("Andrea").build();
    private static final HelloRequest stefano = HelloRequest.newBuilder().setName("Stefano").build();


    private static final StreamObserver<HelloReply> replyPrinter = new StreamObserver<>() {

        @Override
        public void onNext(HelloReply value) {
            System.out.println(value.getMessage());
        }

        @Override
        public void onError(Throwable t) {
            t.printStackTrace();
        }

        @Override
        public void onCompleted() {
            // do nothing
        }
    };

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var channel = ManagedChannelBuilder
                .forAddress("localhost", 10000)
                .usePlaintext()
                .build();

        var blockingClient = GreeterGrpc.newBlockingStub(channel);
        var asyncClient = GreeterGrpc.newFutureStub(channel);
        var client = GreeterGrpc.newStub(channel);

        // TODO say hello to people, individually, via each sort of client

        // TODO say hello to people, individually

        channel.shutdown();
        channel.awaitTermination(1, TimeUnit.SECONDS);
    }
}
