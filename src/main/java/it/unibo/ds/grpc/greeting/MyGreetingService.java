package it.unibo.ds.grpc.greeting;

import io.grpc.stub.StreamObserver;

import java.util.stream.Collectors;

public class MyGreetingService extends GreeterGrpc.GreeterImplBase {

    private HelloReply greet(String name) {
        return HelloReply.newBuilder().setMessage("Hello " + name + "!").build();
    }

    private HelloReply greet(HelloRequest request) {
        return greet(request.getName());
    }

    private HelloReply greet(ArrayOfHelloRequests request) {
        var message = request.getItemsList().stream()
                .map(HelloRequest::getName)
                .collect(Collectors.joining(", "));
        return greet(message);
    }

    // TODO implement callbacks by overriding them
}
